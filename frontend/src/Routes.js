import { Routes, Route } from 'react-router-dom';
import Home from './components/pages/Home';
import Contact from './components/pages/Contact';
import Company from './components/pages/Company';

import NewProject from './components/pages/NewProject';
import Projects from './components/pages/Projects';
import NoMatch from './components/pages/NoMatch';
import Project from './components/pages/Project';

const ListRoutes = () => {
  return (
    <Routes>
      <Route path='/' element={<Home />} />
      <Route path='contact' element={<Contact />} />
      <Route path='company' element={<Company />} />
      <Route path='newproject' element={<NewProject />} />
      <Route path='/project/:id' element={<Project />} />
      <Route path='projects' element={<Projects />} />
      <Route path='*' element={<NoMatch />} />
    </Routes>
  );
};

export default ListRoutes;
