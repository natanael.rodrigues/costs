import Container from './components/layout/Container';
import ListRoutes from './Routes';
import Navbar from './components/layout/Navbar';
import Footer from './components/layout/Footer';

function App() {
  return (
    <>
      <Navbar />
      <Container customClass='min-height'>
        <ListRoutes />
      </Container>
      <Footer />
    </>
  );
}

export default App;
