import { Link, Outlet } from "react-router-dom";

const Layout = () => {
    return (
        <div>
        <h1>teste</h1>
        console.log('teste')
        {/* A "layout route" is a good place to put markup you want to
            share across all the pages on your site, like navigation. */}
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/contato">Contato</Link>
            </li>
          
            <li>
              <Link to="/nothing-here">Nothing Here</Link>
            </li>
          </ul>

  
        <hr />

        <Outlet />
      </div>
    );
  }

  export default Layout