import { useNavigate } from 'react-router-dom';

import ProjectForm from '../../project/ProjectForm';
import styles from './Styles.module.css';
const NewProject = () => {
  const navegate = useNavigate();

  const createPost = (project) => {
    project.cost = 0;
    project.services = [];

    fetch('http://localhost:5000/projects', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify(project),
    })
      .then((resp) => resp.json())
      .then((data) => {
        console.log(data);
        navegate('/projects', {
          state: { message: 'Project criado com sucesso' },
        });
      })
      .catch((error) => console.log(error));
  };

  return (
    <div className={styles.newproject_container}>
      <h1>Criar projeto</h1>
      <p>Crie seu projeto para depois adicionar os serviços</p>
      <ProjectForm handleSubmit={createPost} btnText='Criar projeto' />
    </div>
  );
};

export default NewProject;
