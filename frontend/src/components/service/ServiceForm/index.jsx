import styles from './Styles.module.css'

import { useState } from 'react'
import Input from '../../form/Input'
import SubmitButton from '../../form/SubmitButton'


const ServiceForm = ({ handleSubmit, btnText, projectData }) => {

  const [service, setService] = useState({})

  const submit = (e) => {
    e.preventDefault();
    projectData.services.push(service)
    handleSubmit(projectData)

  }

  const HandleChange = (e) => {
    setService({...service, [e.target.name] : e.target.value})
  }

  return (
    <form onSubmit={submit} className={styles.form}>
      <Input 
        type="text"
        text="Nome do Serviço"
        name="name"
        placeholder="Insira o nome do serviço"
        handleOnChange={HandleChange}
        
      />
      <Input 
        type="number"
        text="Custo do Serviço"
        name="cost"
        placeholder="Insira o valor total"
        handleOnChange={HandleChange}
        
      />
      <Input 
        type="text"
        text="Descrição do Serviço"
        name="description"
        placeholder="Descreva o serviço"
        handleOnChange={HandleChange}
      />
      <SubmitButton 
        text={btnText}
      />
    </form>
  );
};

export default ServiceForm;
