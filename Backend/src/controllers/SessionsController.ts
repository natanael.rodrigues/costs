import { Request, Response } from 'express';

import { compare } from 'bcryptjs';
import { sign } from 'jsonwebtoken';
import connection from '../database/connection';
import authConfig from '../config/auth';

export class SessionsController {
  async post(request: Request, response: Response) {
    const { email, password } = request.body;

    const user = await connection('users').where('email', email).first();

    if (!user) {
      return response.status(400).json({ message: 'Credential not found' });
    }

    const comparePassword = await compare(password, user.password);

    if (!comparePassword) {
      return response.status(400).json({ message: 'Credential not found' });
    }

    // https://www.md5hashgenerator.com para gerar uma chave.
    const token = sign(
      {
        userId: user.id,
        userName: user.name,
      },
      String(authConfig.jwt.secret),
      {
        subject: String(user.id),
        expiresIn: String(authConfig.jwt.expiresIn),
      }
    );

    return response.status(200).json({ user, token });
  }
}
