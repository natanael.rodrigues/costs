import { Request, Response } from 'express';
import { prisma } from '../database/Prisma';
import connection from '../database/connection';

export class LocationController {
  async index(request: Request, response: Response) {
    const { city, uf, items } = request.query;
    if (city && uf && items) {
      const listItems: Number[] = String(items)
        .split(',')
        .map((item) => {
          return Number(item.trim());
        });

      const locations = await connection('locations')
        .join(
          'location_items',
          'locations.id',
          '=',
          'location_items.location_id'
        )
        .whereIn('location_items.item_id', listItems)
        .where('city', String(city))
        .where('uf', String(uf))
        .distinct()
        .select('locations.*');

      return response.status(200).json({ locations });
    } else {
      const locations = await prisma.location.findMany({});
      return response.status(200).json({ locations });
    }
  }

  async post(request: Request, response: Response) {
    const { name, email, whatsapp, latitude, longitude, city, uf, items } =
      request.body;

    const location = {
      image: 'face-image.png',
      name,
      email,
      whatsapp,
      latitude,
      longitude,
      city,
      uf,
    };

    const newLocation = await prisma.location.create({
      data: location,
    });

    return response.json(newLocation);

    // ESTÁ DANDO ERRO, ANALISAR NOVAMENTE DEPOIS.
    // const locationItens = itens.map(async (item_id: number) => {
    //   const selectedItem = await transaction('items')
    //     .where({ id: item_id })
    //     .first();

    //   if (!selectedItem) {
    //     return response.status(400).json({ message: 'Item not found' });
    //   }

    //   return { item_id, location_id };
    // });

    // const locationItens = itens.map(async (item_id: number) => {
    //   return { item_id, location_id };
    // });

    // await transaction('location_items').insert(locationItens);

    // await transaction.commit();

    // return response
    //   .json({
    //     id: location_id,
    //     ...location,
    //  })
    //  .status(200);
  }
}
