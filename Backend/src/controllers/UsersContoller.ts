import { Request, Response } from 'express';
import { hash } from 'bcryptjs';
import connection from '../database/connection';

export class UsersController {
  async index(request: Request, response: Response) {
    const users = await connection('users').select('*');

    return response.json(users).status(200);
  }

  async post(request: Request, response: Response) {
    const { name, email, password } = request.body;

    const passwordHashed = await hash(password, 8);

    const user = { name, email, password: passwordHashed };

    const newIds = await connection('users').insert(user);

    return response.json({ id: newIds[0], ...user }).status(200);
  }
}
