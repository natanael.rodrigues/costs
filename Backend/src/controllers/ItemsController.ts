import { Request, Response } from 'express';
import { prisma } from '../database/Prisma';

export class ItemsController {
  async index(request: Request, response: Response) {
    const items = await prisma.item.findMany({});

    const serializedItems = items.map((item) => {
      return {
        id: item.id,
        title: item.title,
        image_url: `${process.env.BASE_URL}uploads/${item.image}`,
      };
    });

    return response.status(200).json({ items: serializedItems });
  }
}
