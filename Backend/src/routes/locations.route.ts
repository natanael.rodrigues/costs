import { Router, Request, Response, NextFunction } from 'express';
import connection from '../database/connection';
import multer from 'multer';
import multerConfig from '../config/multer';
import { celebrate, Joi } from 'celebrate';

import isAuthenticated from '../middlewares/isAuthehticated';
import { LocationController } from '../controllers/LocationController';

const locationsRouter = Router();

//locationsRouter.use(isAuthenticated);

const uploads = multer(multerConfig);

locationsRouter.get('/', new LocationController().index);

locationsRouter.post(
  '/',
  celebrate(
    {
      body: Joi.object().keys({
        name: Joi.string().required(),
        email: Joi.string().required().email(),
        whatsapp: Joi.string().required(),
        latitude: Joi.number().required(),
        longitude: Joi.number().required(),
        city: Joi.string().required(),
        uf: Joi.string().required().max(2),
        items: Joi.array().required(),
      }),
    },
    {
      abortEarly: false,
    }
  ),
  new LocationController().post
);

locationsRouter.get('/locations/:id', async (request, response) => {
  const { id } = request.params;
  const location = await connection('locations')
    .where('id', id)
    .select('*')
    .first();

  if (!location) {
    return response.status(400).json({ message: 'Location not found' });
  }

  const items = await connection('items')
    .join('location_items', 'items.id', '=', 'location_items.item_id')
    .where('location_items.location_id', id)
    .select('items.title');

  return response.status(200).json({ location, items });
});
locationsRouter.put(
  '/locations/:id',
  uploads.single('image'),
  async (request, response) => {
    const { id } = request.params;
    const image = request.file?.filename;
    const location = await connection.where('id', id).first();

    if (!location) {
      return response.status(400).json({ message: 'location not found' });
    }

    const locationUpdated = { ...location, image };

    await connection('locations').update(locationUpdated).where('id', id);

    return response.status(200).json(locationUpdated);
  }
);

export default locationsRouter;
