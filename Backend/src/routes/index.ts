import { Router, Request, Response, NextFunction } from 'express';
import itemsRouter from './items.router';
import locationsRouter from './locations.route';
import usersRouter from './users.route';
import sessionsRouter from './sessions.route';
import { logRouter } from '../middlewares/logRouter';

const routes = Router();

routes.use(logRouter);

routes.get('/ping', logRouter, (request, response) => {
  return response
    .json({
      message: 'pong',
    })
    .status(200);
});

routes.use(itemsRouter);
routes.use('/locations', locationsRouter);
routes.use('/user', usersRouter);
routes.use('/sessions', sessionsRouter);

export default routes;
