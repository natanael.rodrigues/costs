import { Router } from 'express';

import { UsersController } from '../controllers/UsersContoller';

const usersRouter = Router();

usersRouter.get('/', new UsersController().index);
usersRouter.post('/', new UsersController().post);

export default usersRouter;
