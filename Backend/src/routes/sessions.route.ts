import { Router } from 'express';
import { SessionsController } from '../controllers/SessionsController';

const sessionsRoute = Router();

sessionsRoute.post('/', new SessionsController().post);

export default sessionsRoute;
