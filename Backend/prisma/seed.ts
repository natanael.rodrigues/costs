import { prisma } from "../src/database/Prisma";

import itemsSeed from "./seeds/itemsSeed";

async function main(){
    for(let i of itemsSeed.items){
        await prisma.item.create({
            data:{
                title: i.title,
                image: i.image
            }
        })
    }
}

main()
  .then(async () => {
    await prisma.$disconnect()
  })
  .catch(async (e) => {
    console.error(e)
    await prisma.$disconnect()
    process.exit(1)
  })