/*
  Warnings:

  - You are about to drop the column `location_ItemId` on the `Location` table. All the data in the column will be lost.
  - You are about to drop the column `location_ItemId` on the `Item` table. All the data in the column will be lost.
  - Added the required column `itemId` to the `Location_Item` table without a default value. This is not possible if the table is not empty.
  - Added the required column `locationId` to the `Location_Item` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Location" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT,
    "email" TEXT,
    "whatsapp" TEXT,
    "latitude" TEXT,
    "longitude" TEXT,
    "city" TEXT,
    "uf" TEXT,
    "created_at" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);
INSERT INTO "new_Location" ("city", "created_at", "email", "id", "latitude", "longitude", "name", "uf", "whatsapp") SELECT "city", "created_at", "email", "id", "latitude", "longitude", "name", "uf", "whatsapp" FROM "Location";
DROP TABLE "Location";
ALTER TABLE "new_Location" RENAME TO "Location";
CREATE TABLE "new_Item" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "title" TEXT,
    "image" TEXT,
    "created_at" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);
INSERT INTO "new_Item" ("created_at", "id", "image", "title") SELECT "created_at", "id", "image", "title" FROM "Item";
DROP TABLE "Item";
ALTER TABLE "new_Item" RENAME TO "Item";
CREATE TABLE "new_Location_Item" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "locationId" TEXT NOT NULL,
    "itemId" TEXT NOT NULL,
    "created_at" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT "Location_Item_locationId_fkey" FOREIGN KEY ("locationId") REFERENCES "Location" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Location_Item_itemId_fkey" FOREIGN KEY ("itemId") REFERENCES "Item" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_Location_Item" ("created_at", "id") SELECT "created_at", "id" FROM "Location_Item";
DROP TABLE "Location_Item";
ALTER TABLE "new_Location_Item" RENAME TO "Location_Item";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
