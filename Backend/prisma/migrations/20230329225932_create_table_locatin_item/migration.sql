-- CreateTable
CREATE TABLE "Location_Item" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "created_at" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Item" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "title" TEXT,
    "image" TEXT,
    "created_at" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "location_ItemId" TEXT,
    CONSTRAINT "Item_location_ItemId_fkey" FOREIGN KEY ("location_ItemId") REFERENCES "Location_Item" ("id") ON DELETE SET NULL ON UPDATE CASCADE
);
INSERT INTO "new_Item" ("created_at", "id", "image", "title") SELECT "created_at", "id", "image", "title" FROM "Item";
DROP TABLE "Item";
ALTER TABLE "new_Item" RENAME TO "Item";
CREATE TABLE "new_Location" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT,
    "email" TEXT,
    "whatsapp" TEXT,
    "latitude" TEXT,
    "longitude" TEXT,
    "city" TEXT,
    "uf" TEXT,
    "created_at" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "location_ItemId" TEXT,
    CONSTRAINT "Location_location_ItemId_fkey" FOREIGN KEY ("location_ItemId") REFERENCES "Location_Item" ("id") ON DELETE SET NULL ON UPDATE CASCADE
);
INSERT INTO "new_Location" ("city", "created_at", "email", "id", "latitude", "longitude", "name", "uf", "whatsapp") SELECT "city", "created_at", "email", "id", "latitude", "longitude", "name", "uf", "whatsapp" FROM "Location";
DROP TABLE "Location";
ALTER TABLE "new_Location" RENAME TO "Location";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
