export default {
    items : [
        { title: 'Papeis e Papelão', image: 'papel.png' },
        { title: 'Vidros e Lampadas', image: 'vidro.png' },
        { title: 'Óleo de Cozinha', image: 'oleo.png' },
        { title: 'Resodios Orgânicos', image: 'organico.png' },
        { title: 'Baterias e Pilhas', image: 'bateria.png' },
        { title: 'Eletrônicos', image: 'eletronicos.png' }
    ]
}
