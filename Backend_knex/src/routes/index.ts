import { Router, Request, Response, NextFunction } from 'express';
import itemsRouter from './items.router';
import locationsRouter from './locations.route';
import usersRouter from './users.route';
import sessionsRouter from './sessions.route';

const routes = Router();

const logRoutes = (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  const { method, url } = request;

  const route = `[${method.toUpperCase()}] - ${url}`;
  console.log(route);

  return next();
};

routes.get('/ping', logRoutes, (request, response) => {
  return response
    .json({
      message: 'pong',
    })
    .status(200);
});

routes.use(itemsRouter);
routes.use(locationsRouter);
routes.use('/user', usersRouter);
routes.use('/sessions', sessionsRouter);

export default routes;
