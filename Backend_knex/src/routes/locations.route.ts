import { Router, Request, Response, NextFunction } from 'express';
import connection from '../database/connection';
import multer from 'multer';
import multerConfig from '../config/multer';
import { celebrate, Joi } from 'celebrate';

import isAuthenticated from '../middlewares/isAuthehticated';

const locationsRouter = Router();

locationsRouter.use(isAuthenticated);

const logRoutes = (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  const { method, url } = request;

  const route = `[${method.toUpperCase()}] - ${url}`;
  console.log(route);

  return next();
};

const uploads = multer(multerConfig);

locationsRouter.get('/locations', logRoutes, async (request, response) => {
  const { city, uf, items } = request.query;
  if (city && uf && items) {
    const listItems: Number[] = String(items)
      .split(',')
      .map((item) => {
        return Number(item.trim());
      });

    const locations = await connection('locations')
      .join('location_items', 'locations.id', '=', 'location_items.location_id')
      .whereIn('location_items.item_id', listItems)
      .where('city', String(city))
      .where('uf', String(uf))
      .distinct()
      .select('locations.*');

    return response.status(200).json({ locations });
  } else {
    const locations = await connection('locations').select('*');
    return response.status(200).json({ locations });
  }
});

locationsRouter.post(
  '/locations',
  logRoutes,
  celebrate(
    {
      body: Joi.object().keys({
        name: Joi.string().required(),
        email: Joi.string().required().email(),
        whatsapp: Joi.string().required(),
        latitude: Joi.number().required(),
        logitude: Joi.number().required(),
        city: Joi.string().required(),
        uf: Joi.string().required().max(2),
        items: Joi.string().required(),
      }),
    },
    {
      abortEarly: false,
    }
  ),
  async (request, response) => {
    const { name, email, whatsapp, latitude, longitude, city, uf, itens } =
      request.body;

    const location = {
      image: 'face-image.png',
      name,
      email,
      whatsapp,
      latitude,
      longitude,
      city,
      uf,
      //itens,
    };

    const transaction = await connection.transaction();

    const newIds = await transaction('locations').insert(location);
    const location_id = newIds[0];

    // ESTÁ DANDO ERRO, ANALISAR NOVAMENTE DEPOIS.
    // const locationItens = itens.map(async (item_id: number) => {
    //   const selectedItem = await transaction('items')
    //     .where({ id: item_id })
    //     .first();

    //   if (!selectedItem) {
    //     return response.status(400).json({ message: 'Item not found' });
    //   }

    //   return { item_id, location_id };
    // });

    const locationItens = itens.map(async (item_id: number) => {
      return { item_id, location_id };
    });

    await transaction('location_items').insert(locationItens);

    await transaction.commit();

    return response
      .json({
        id: location_id,
        ...location,
      })
      .status(200);
  }
);

locationsRouter.get('/locations/:id', logRoutes, async (request, response) => {
  const { id } = request.params;
  const location = await connection('locations')
    .where('id', id)
    .select('*')
    .first();

  if (!location) {
    return response.status(400).json({ message: 'Location not found' });
  }

  const items = await connection('items')
    .join('location_items', 'items.id', '=', 'location_items.item_id')
    .where('location_items.location_id', id)
    .select('items.title');

  return response.status(200).json({ location, items });
});
locationsRouter.put(
  '/locations/:id',
  uploads.single('image'),
  logRoutes,
  async (request, response) => {
    const { id } = request.params;
    const image = request.file?.filename;
    const location = await connection.where('id', id).first();

    if (!location) {
      return response.status(400).json({ message: 'location not found' });
    }

    const locationUpdated = { ...location, image };

    await connection('locations').update(locationUpdated).where('id', id);

    return response.status(200).json(locationUpdated);
  }
);

export default locationsRouter;
