import { Router, Request, Response, NextFunction } from 'express';
import { compare } from 'bcryptjs';
import { sign } from 'jsonwebtoken';
import connection from '../database/connection';
import authConfig from '../config/auth';

const sessionsRoute = Router();

const logRoutes = (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  const { method, url } = request;

  const route = `[${method.toUpperCase()}] - ${url}`;
  console.log(route);

  return next();
};

sessionsRoute.post('/', logRoutes, async (request, response) => {
  const { email, password } = request.body;

  const user = await connection('users').where('email', email).first();

  if (!user) {
    return response.status(400).json({ message: 'Credential not found' });
  }

  const comparePassword = await compare(password, user.password);

  if (!comparePassword) {
    return response.status(400).json({ message: 'Credential not found' });
  }

  // https://www.md5hashgenerator.com para gerar uma chave.
  const token = sign(
    {
      userId: user.id,
      userName: user.name,
    },
    String(authConfig.jwt.secret),
    {
      subject: String(user.id),
      expiresIn: String(authConfig.jwt.expiresIn),
    }
  );

  return response.status(200).json({ user, token });
});

export default sessionsRoute;
