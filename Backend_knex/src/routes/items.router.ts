import { Router, Request, Response, NextFunction } from 'express';
import connection from '../database/connection';

const itemsRouter = Router();

const logRoutes = (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  const { method, url } = request;

  const route = `[${method.toUpperCase()}] - ${url}`;
  console.log(route);

  return next();
};

itemsRouter.get('/items', logRoutes, async (request, response) => {
  const items = await connection('items').select('*');

  const serializedItems = items.map((item) => {
    return {
      id: item.id,
      title: item.title,
      image_url: `${process.env.BASE_URL}uploads/${item.image}`,
    };
  });

  return response
    .json({
      items: serializedItems,
    })
    .status(200);
});

export default itemsRouter;
