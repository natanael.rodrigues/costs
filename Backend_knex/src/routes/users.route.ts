import { Router, Request, Response, NextFunction } from 'express';
import { hash } from 'bcryptjs';
import connection from '../database/connection';

const usersRouter = Router();

const logRoutes = (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  const { method, url } = request;

  const route = `[${method.toUpperCase()}] - ${url}`;
  console.log(route);

  return next();
};

usersRouter.get('/', logRoutes, async (request, response) => {
  const users = await connection('users').select('*');

  return response.json(users).status(200);
});

usersRouter.post('/', logRoutes, async (request, response) => {
  const { name, email, password } = request.body;

  const passwordHashed = await hash(password, 8);

  const user = { name, email, password: passwordHashed };

  const newIds = await connection('users').insert(user);

  return response.json({ id: newIds[0], ...user }).status(200);
});

export default usersRouter;
