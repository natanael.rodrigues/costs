import express from 'express';
import dotenv from 'dotenv';
import corts from 'cors';
import path from 'path';

import { errors } from 'celebrate';

import routes from './routes';

dotenv.config();

const app = express();
app.use(corts());
// app.use(cors({
//   origin:['dalmark.com.br','systeam.com.br']
// }))

const port = process.env.PORT ? process.env.PORT : 3001;
app.use(express.json());

//app.use(logRoutes); middleware para todas as rotas.

app.use(routes);
app.use('/uploads', express.static(path.resolve(__dirname, '..', 'uploads')));

// para retornar os erros do celebrate
app.use(errors());

app.listen(port, () => {
  console.log(`Server running in port ${port}`);
});
