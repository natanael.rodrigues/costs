### Frontend

npx create-react-app front
npm install json-server
npm install react-icons
npm install react-router-dom
npm install uuid

proxima aula: https://www.youtube.com/watch?v=pOVyVivyfok&list=PLnDvRpP8BneyVA0SZ2okm-QBojomniQVO&index=41

### Backend.

npm init -y
npm i express
npm i nodemon -D
npm i typescript -D
npm i --save-dev @types/express
npm i ts-node-dev -D
(ORM)
https://knexjs.org/guide/
npm i knex --save
para o banco
npm i sqlite3
npm i cors
npm i --save-dev @types/cors
npm i multer // para upload de imagens.
npm i --save-dev @types/multer -D
npm i celebrate // middleware para validação de dados.
// Documentação de erro do Joi usado no Celebrate. joi.dev
npm i bcryptjs
npm i --save-dev @types/bcryptjs -D
npm i jsonwebtoken
npm i --save-dev @types/jsonwebtoken -D

##instalar o prisma.
npm i prisma -D
npx prisma init
Alterar o provider no prisma/schema.prisma
cria a modelagem no schema.prisma
comando npx prisma migrate dev
comando seed npx prisma db seed

### estudar

npm i -D typescript @types/node tsx tsup
tsc -init
mudar target para es2020
\*\* ver a usabilidade do fastify e zod
referencia https://www.youtube.com/watch?v=pmXfvd6Zqg4

Sobre o Seed no prisma -> https://youtu.be/2LwTUIqjbPo
