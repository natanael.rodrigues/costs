import { useNavigate, useLocation } from "react-router-dom";
import ProjectForm from "../project/ProjectForm";
import Message from "../Layout/Message";
import { useState } from "react";

import styles from "./NewProject.module.css";

function NewProject() {
  const [msg, setMsg] = useState();
  const [type, setType] = useState();

  const navigate = useNavigate();
  let message;
  function createPost(project) {
    //Inicialize cost and services
    project.cost = 0;
    project.services = [];
    setMsg("");

    if (!project.name || !project.budget) {
      setMsg(
        "Os campos Nome do Projeto, Orçamento e Categoria são obrigatórios!"
      );
      setType("error");
      return;
    }

    fetch("http://localhost:5000/project", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(project),
    })
      .then((resp) => resp.json())
      .then((data) => {
        //Verificar como recupero o valor do objeto do navigate
        navigate("/projects", {
          state: { Message: "Projeto criado com Sucesso!" },
        });
      })
      .catch((err) => console.log(err));
  }

  return (
    <div className={styles.newproject_container}>
      {Message && <Message type={type} msg={msg} />}
      <h1>Criar Projeto</h1>
      <p>Crie seu projeto para depois adicionar os serviços</p>
      <ProjectForm handleSubmit={createPost} btnText="Criar Projeto" />
    </div>
  );
}

export default NewProject;
